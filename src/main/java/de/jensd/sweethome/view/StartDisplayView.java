package de.jensd.sweethome.view;

import com.guigarage.responsive.ResponsiveHandler;
import java.util.Locale;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class StartDisplayView extends Application {

    @Override
    public void init() throws Exception {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Override
    public void start(Stage primaryStage) {
        DisplayView displayView = new DisplayView();
        Scene scene = new Scene(displayView, 800.0, 480.0, Color.GRAY);
        String css = getClass().getResource("display.css").toExternalForm();
        scene.getStylesheets().addAll(css);
        primaryStage.setTitle("SweetHome Display");
        primaryStage.setScene(scene);
//        primaryStage.setFullScreen(true);
        primaryStage.show();
        Platform.setImplicitExit(false);

        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            event.consume();
            System.exit(0);
        });

        displayView.onConnect();
        ResponsiveHandler.addResponsiveToWindow(primaryStage);
    }
}
