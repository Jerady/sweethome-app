package de.jensd.sweethome.view;

import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.charm.down.common.SettingService;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Settings {

    public static enum AppContext {

        HOME, WORLD;
    }

    private static Settings me;
    private final SettingService settingsService = PlatformFactory.getPlatform().getSettingService();

    public final static String MQTT_BROKER_ADDRESS_HOME = "mqttBrokerAddressHome";
    public final static String MQTT_BROKER_PORT_HOME = "mqttBrokerPortHome";
    public final static String MQTT_BROKER_ADDRESS_WORLD = "mqttBrokerAddressWorld";
    public final static String MQTT_BROKER_PORT_WORLD = "mqttBrokerPortWorld";
    public final static String MQTT_BROKER_USERNAME_WORLD = "mqttBrokerUsernameWorld";
    public final static String MQTT_BROKER_PASSSWORD_WORLD = "mqttBrokerPasswordWorld";
    public final static String MQTT_BROKER_USESSL_WORLD = "mqttBrokerUseSslWorld";
    public final static String APPLICATION_CONTEXT = "applicationContext";

    private final static String MQTT_BROKER_ADDRESS_DEFAULT_HOME = "192.168.0.61";
    private final static String MQTT_BROKER_PORT_DEFAULT_HOME = "1883";
    private final static String MQTT_BROKER_ADDRESS_DEFAULT_WORLD = "heubaum.synology.me";
    private final static String MQTT_BROKER_PORT_DEFAULT_WORLD = "1883";
    private final static String MQTT_BROKER_USERNAME_DEFAULT_WORLD = "jde";
    private final static String MQTT_BROKER_PASSWORD_DEFAULT_WORLD = "jde";
    private final static String MQTT_BROKER_USESSL_DEFAULT_WORLD = "false";
    private final static String APPLICATION_CONTEXT_DEFAULT = "HOME";

    private StringProperty mqttBrokerAddressHome;
    private StringProperty mqttBrokerPortHome;
    private StringProperty mqttBrokerAddressWorld;
    private StringProperty mqttBrokerPortWorld;
    private StringProperty mqttBrokerUsernameWorld;
    private StringProperty mqttBrokerPasswordWorld;
    private BooleanProperty mqttBrokerUseSslWorld;
    private ObjectProperty<AppContext> applicationContext;

    private Settings() {
        setDefaultValuesIfNeeded();
    }

    public static Settings getInstance() {
        if (me == null) {
            me = new Settings();
        }
        return me;
    }

    /*
     Properties
     */
    public StringProperty mqttBrokerAddressHomeProperty() {
        if (mqttBrokerAddressHome == null) {
            mqttBrokerAddressHome = new SimpleStringProperty();
            setMqttBrokerAddressHome(settingsService.retrieve(MQTT_BROKER_ADDRESS_HOME));
        }
        return mqttBrokerAddressHome;
    }

    public String getMqttBrokerAddressHome() {
        return mqttBrokerAddressHomeProperty().getValue();
    }

    public void setMqttBrokerAddressHome(String mqttBrokerAddressHome) {
        mqttBrokerAddressHomeProperty().setValue(mqttBrokerAddressHome);
    }

    public StringProperty mqttBrokerPortHomeProperty() {
        if (mqttBrokerPortHome == null) {
            mqttBrokerPortHome = new SimpleStringProperty();
            setMqttBrokerPortHome(settingsService.retrieve(MQTT_BROKER_PORT_HOME));
        }
        return mqttBrokerPortHome;
    }

    public String getMqttBrokerPortHome() {
        return mqttBrokerPortHomeProperty().getValue();
    }

    public void setMqttBrokerPortHome(String mqttBrokerPort) {
        mqttBrokerPortHomeProperty().setValue(mqttBrokerPort);
    }

    public StringProperty mqttBrokerAddressWorldProperty() {
        if (mqttBrokerAddressWorld == null) {
            mqttBrokerAddressWorld = new SimpleStringProperty();
            setMqttBrokerAddressWorld(settingsService.retrieve(MQTT_BROKER_ADDRESS_WORLD));
        }
        return mqttBrokerAddressWorld;
    }

    public String getMqttBrokerAddressWorld() {
        return mqttBrokerAddressWorldProperty().getValue();
    }

    public void setMqttBrokerAddressWorld(String mqttBrokerAddressWorld) {
        mqttBrokerAddressWorldProperty().setValue(mqttBrokerAddressWorld);
    }

    public StringProperty mqttBrokerPortWorldProperty() {
        if (mqttBrokerPortWorld == null) {
            mqttBrokerPortWorld = new SimpleStringProperty();
            setMqttBrokerPortWorld(settingsService.retrieve(MQTT_BROKER_PORT_WORLD));
        }
        return mqttBrokerPortWorld;
    }

    public String getMqttBrokerPortWorld() {
        return mqttBrokerPortWorldProperty().getValue();
    }

    public void setMqttBrokerPortWorld(String mqttBrokerPortWorld) {
        mqttBrokerPortWorldProperty().setValue(mqttBrokerPortWorld);
    }

    public StringProperty mqttBrokerUsernameWorldProperty() {
        if (mqttBrokerUsernameWorld == null) {
            mqttBrokerUsernameWorld = new SimpleStringProperty();
            setMqttBrokerUsernameWorld(settingsService.retrieve(MQTT_BROKER_USERNAME_WORLD));
        }
        return mqttBrokerUsernameWorld;
    }

    public String getMqttBrokerUsernameWorld() {
        return mqttBrokerUsernameWorldProperty().getValue();
    }

    public void setMqttBrokerUsernameWorld(String mqttBrokerUsernameWorld) {
        mqttBrokerUsernameWorldProperty().setValue(mqttBrokerUsernameWorld);
    }

    public StringProperty mqttBrokerPasswordWorldProperty() {
        if (mqttBrokerPasswordWorld == null) {
            mqttBrokerPasswordWorld = new SimpleStringProperty();
            setMqttBrokerPasswordWorld(settingsService.retrieve(MQTT_BROKER_PASSSWORD_WORLD));
        }
        return mqttBrokerPasswordWorld;
    }

    public String getMqttBrokerPasswordWorld() {
        return mqttBrokerPasswordWorldProperty().getValue();
    }

    public void setMqttBrokerPasswordWorld(String mqttBrokerPasswordWorld) {
        mqttBrokerPasswordWorldProperty().setValue(mqttBrokerPasswordWorld);
    }

    public BooleanProperty mqttBrokerUseSslWorldProperty() {
        if (mqttBrokerUseSslWorld == null) {
            mqttBrokerUseSslWorld = new SimpleBooleanProperty();
            setMqttBrokerUseSslWorld(Boolean.parseBoolean(settingsService.retrieve(MQTT_BROKER_USESSL_WORLD)));
        }
        return mqttBrokerUseSslWorld;
    }

    public Boolean getMqttBrokerUseSslWorld() {
        return mqttBrokerUseSslWorldProperty().getValue();
    }

    public void setMqttBrokerUseSslWorld(Boolean mqttBrokerUseSslWorld) {
        mqttBrokerUseSslWorldProperty().setValue(mqttBrokerUseSslWorld);
    }

    public void setMqttBrokerUseSslWorld(String mqttBrokerUseSslWorld) {
        mqttBrokerUseSslWorldProperty().setValue(Boolean.parseBoolean(mqttBrokerUseSslWorld));
    }

    public ObjectProperty<AppContext> applicationContextProperty() {
        if(applicationContext == null){
            applicationContext = new SimpleObjectProperty<>(AppContext.HOME);
        }
        return applicationContext;
    }

    public AppContext getApplicationContext() {
        return applicationContextProperty().getValue();
    }

    public void setApplicationContext(AppContext applicationContext) {
        applicationContextProperty().setValue(applicationContext);
    }

    //------------------
    private void setDefaultValuesIfNeeded() {
        if (settingsService.retrieve(MQTT_BROKER_ADDRESS_HOME) == null
                || settingsService.retrieve(MQTT_BROKER_ADDRESS_HOME).isEmpty()) {
            setMqttBrokerAddressHome(MQTT_BROKER_ADDRESS_DEFAULT_HOME);
        }
        if (settingsService.retrieve(MQTT_BROKER_PORT_HOME) == null
                || settingsService.retrieve(MQTT_BROKER_PORT_HOME).isEmpty()) {
            setMqttBrokerPortHome(MQTT_BROKER_PORT_DEFAULT_HOME);
        }
        if (settingsService.retrieve(MQTT_BROKER_ADDRESS_WORLD) == null
                || settingsService.retrieve(MQTT_BROKER_ADDRESS_WORLD).isEmpty()) {
            setMqttBrokerAddressWorld(MQTT_BROKER_ADDRESS_DEFAULT_WORLD);
        }
        if (settingsService.retrieve(MQTT_BROKER_PORT_WORLD) == null
                || settingsService.retrieve(MQTT_BROKER_PORT_WORLD).isEmpty()) {
            setMqttBrokerPortWorld(MQTT_BROKER_PORT_DEFAULT_WORLD);
        }
        if (settingsService.retrieve(MQTT_BROKER_USERNAME_WORLD) == null
                || settingsService.retrieve(MQTT_BROKER_USERNAME_WORLD).isEmpty()) {
            setMqttBrokerUsernameWorld(MQTT_BROKER_USERNAME_DEFAULT_WORLD);
        }
        if (settingsService.retrieve(MQTT_BROKER_PASSSWORD_WORLD) == null
                || settingsService.retrieve(MQTT_BROKER_PASSSWORD_WORLD).isEmpty()) {
            setMqttBrokerPasswordWorld(MQTT_BROKER_PASSWORD_DEFAULT_WORLD);
        }
        if (settingsService.retrieve(MQTT_BROKER_USESSL_WORLD) == null
                || settingsService.retrieve(MQTT_BROKER_USESSL_WORLD).isEmpty()) {
            setMqttBrokerUseSslWorld(MQTT_BROKER_USESSL_DEFAULT_WORLD);
        }
        if (settingsService.retrieve(APPLICATION_CONTEXT) == null
                || settingsService.retrieve(APPLICATION_CONTEXT).isEmpty()) {
            setApplicationContext(AppContext.valueOf(APPLICATION_CONTEXT_DEFAULT));
        }
        saveSettings();
    }

    public void saveSettings() {
        settingsService.store(MQTT_BROKER_ADDRESS_HOME, getMqttBrokerAddressHome());
        settingsService.store(MQTT_BROKER_PORT_HOME, getMqttBrokerPortHome());
        settingsService.store(MQTT_BROKER_ADDRESS_WORLD, getMqttBrokerAddressWorld());
        settingsService.store(MQTT_BROKER_PORT_WORLD, getMqttBrokerPortWorld());
        settingsService.store(MQTT_BROKER_USERNAME_WORLD, getMqttBrokerUsernameWorld());
        settingsService.store(MQTT_BROKER_PASSSWORD_WORLD, getMqttBrokerPasswordWorld());
        settingsService.store(MQTT_BROKER_USESSL_WORLD, getMqttBrokerUseSslWorld().toString());
        settingsService.store(APPLICATION_CONTEXT, getApplicationContext().name());
    }

    public void restoreSettings() {
        setMqttBrokerAddressHome(settingsService.retrieve(MQTT_BROKER_ADDRESS_HOME));
        setMqttBrokerPortHome(settingsService.retrieve(MQTT_BROKER_PORT_HOME));
        setMqttBrokerAddressWorld(settingsService.retrieve(MQTT_BROKER_ADDRESS_WORLD));
        setMqttBrokerPortWorld(settingsService.retrieve(MQTT_BROKER_PORT_WORLD));
        setMqttBrokerUsernameWorld(settingsService.retrieve(MQTT_BROKER_USERNAME_WORLD));
        setMqttBrokerPasswordWorld(settingsService.retrieve(MQTT_BROKER_PASSSWORD_WORLD));
        setMqttBrokerUseSslWorld(settingsService.retrieve(MQTT_BROKER_USESSL_WORLD));
        setApplicationContext(AppContext.valueOf(settingsService.retrieve(APPLICATION_CONTEXT)));
    }

}
