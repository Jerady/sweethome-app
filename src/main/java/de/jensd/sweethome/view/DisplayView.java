/*
 * Copyright (c) 2015, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.view;

import com.gluonhq.charm.down.common.JavaFXPlatform;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.shichimifx.control.Indicator;
import de.jensd.shichimifx.control.OutputConsole;
import de.jensd.shichimifx.control.UVIndexView;
import de.jensd.shichimifx.control.ValueView;
import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.SweetHomeValues;
import de.jensd.sweethome.core.SweethomeTopics;
import de.jensd.sweethome.core.mqtt.SweethomeValuesMQTTAdapter;
import eu.hansolo.fx.regulators.ColorRegulator;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author Jens Deters
 */
public class DisplayView extends VBox {

    @FXML
    private Label lastUpdateLabel;
    @FXML
    private Label connectionStatusLabel;
    @FXML
    private Indicator connectionStatusIndicator;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private ToggleButton displayModeToggleButton;
    @FXML
    private ValueView indoorTempView;
    @FXML
    private ValueView outdoorTempView;
    @FXML
    private ValueView luxVisibleView;
    @FXML
    private ValueView indoorBathroomTempView;
    @FXML
    private ValueView humidityView;
    @FXML
    private ValueView pressureView;
    @FXML
    private TextField mqttBrokerAddressFieldWorld;
    @FXML
    private TextField mqttBrokerPortFieldWorld;
    @FXML
    private TextField mqttBrokerUsernameFieldWorld;
    @FXML
    private PasswordField mqttBrokerPasswordFieldWorld;
    @FXML
    private TextField mqttBrokerAddressFieldHome;
    @FXML
    private TextField mqttBrokerPortFieldHome;
    @FXML
    private CheckBox mqttBrokerUseSslCheckBoxWorld;
    @FXML
    private Button exitButton;
    @FXML
    private ToggleButton contextToggleButton;
    @FXML
    private FontAwesomeIconView contextButtonIconView;
    @FXML
    private UVIndexView uvIndexView;
    @FXML
    private VBox loggingConsoleBox; 
    @FXML
    private OutputConsole loggingTextArea;
    @FXML
    private ProgressIndicator disconnectedIndicator;
    @FXML
    private VBox switchesPane;
    @FXML
    private Label currentConsumptionP1Label;
    @FXML
    private Label currentConsumptionP2Label;
    @FXML
    private Label currentConsumptionP3Label;
    @FXML
    private Label currentIrmsP1Label;
    @FXML
    private Label currentIrmsP2Label;
    @FXML
    private Label currentIrmsP3Label;
    @FXML
    private Label currentPowerConsumptionLabel;
    @FXML
    private Label currentIrmsLabel;
    @FXML
    private ResourceBundle resources;
    @FXML
    private HBox topPane;
    @FXML
    private VBox rainBowTab;
    @FXML
    private ColorRegulator rainbowSelector;

    private SweethomeValuesMQTTAdapter valuesMQTTAdapter;
    private SweetHomeValues valuesDataModel;

    private boolean rainbowSelectorOn;

    public DisplayView() {
        init();
    }

    private void init() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle(getClass().getPackage().getName() + ".display");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("display.fxml"));
        fxmlLoader.setResources(resourceBundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        exitButton.setVisible(!JavaFXPlatform.isIOS());
        topPane.setVisible(false);
        indoorTempView.valueProperty().bind(getValuesDataModel().indoorLivingroomTemperatureProperty().asString("%.1f"));
        outdoorTempView.valueProperty().bind(getValuesDataModel().outdoorBackyardTemperatureProperty().asString("%.1f"));
        luxVisibleView.valueProperty().bind(getValuesDataModel().outdoorBackyardLuxVisibleProperty().asString("%d"));
        indoorBathroomTempView.valueProperty().bind(getValuesDataModel().indoorBathroomTemperatureProperty().asString("%.1f"));
        humidityView.valueProperty().bind(getValuesDataModel().indoorLivingroomHumidityProperty().asString("%,.1f"));
        pressureView.valueProperty().bind(getValuesDataModel().outdoorBackyardPressureProperty().asString("%,.2f"));
        valuesDataModel.lastUpdateProperty().addListener((ObservableValue<? extends LocalDateTime> observable, LocalDateTime oldValue, LocalDateTime newValue) -> {
            lastUpdateLabel.setText(DateTimeFormatter.ofPattern("HH:mm:ss").format(newValue));
        });
        uvIndexView.valueProperty().bind(getValuesDataModel().outdoorBackyardUvIndexProperty());
        mqttBrokerAddressFieldHome.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerAddressHomeProperty());
        mqttBrokerPortFieldHome.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerPortHomeProperty());
        mqttBrokerAddressFieldWorld.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerAddressWorldProperty());
        mqttBrokerPortFieldWorld.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerPortWorldProperty());
        mqttBrokerUsernameFieldWorld.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerUsernameWorldProperty());
        mqttBrokerPasswordFieldWorld.textProperty().bindBidirectional(Settings.getInstance().mqttBrokerPasswordWorldProperty());
        mqttBrokerUseSslCheckBoxWorld.selectedProperty().bindBidirectional(Settings.getInstance().mqttBrokerUseSslWorldProperty());
        switchesPane.setDisable(false);
        disconnectedIndicator.setVisible(true);
        disconnectedIndicator.setProgress(-1);
        disconnectedIndicator.visibleProperty().bind(getValuesMQTTAdapter().connectedProperty().not());
        switchesPane.disableProperty().bind(getValuesMQTTAdapter().connectedProperty().not());
        currentConsumptionP1Label.textProperty().bind(getValuesDataModel().currentConsumptionP1Property().asString("%,.2f"));
        currentConsumptionP2Label.textProperty().bind(getValuesDataModel().currentConsumptionP2Property().asString("%,.2f"));
        currentConsumptionP3Label.textProperty().bind(getValuesDataModel().currentConsumptionP3Property().asString("%,.2f"));
        currentIrmsP1Label.textProperty().bind(getValuesDataModel().currentIrmsP1Property().asString("%,.2f"));
        currentIrmsP2Label.textProperty().bind(getValuesDataModel().currentIrmsP2Property().asString("%,.2f"));
        currentIrmsP3Label.textProperty().bind(getValuesDataModel().currentIrmsP3Property().asString("%,.2f"));
        currentPowerConsumptionLabel.textProperty().bind(getValuesDataModel().currentConsumptionP1Property().add(getValuesDataModel().currentConsumptionP2Property()).add(getValuesDataModel().currentConsumptionP3Property()).asString("%,.2f"));
        currentIrmsLabel.textProperty().bind(getValuesDataModel().currentIrmsP1Property().add(getValuesDataModel().currentIrmsP2Property()).add(getValuesDataModel().currentIrmsP3Property()).asString("%,.2f"));

        getValuesMQTTAdapter().connectionStatusProperty().addListener((ObservableValue<? extends SweethomeValuesMQTTAdapter.ConnectionStatus> observable, SweethomeValuesMQTTAdapter.ConnectionStatus oldValue, SweethomeValuesMQTTAdapter.ConnectionStatus newValue) -> {
            switch (newValue) {
                case CONNECTED:
                    showStatusConnected();
                    break;
                case DISCONNECTED:
                    showStatusDisconnected();
                    break;
                case CONNECTING:
                    showStatusConnecting();
                    break;
                case UNDEFINED:
                    showStatusDisconnected();
                    break;
            }
        });
        loggingTextArea = new OutputConsole(2000);
        VBox.setVgrow(loggingTextArea, Priority.ALWAYS);
        loggingConsoleBox.getChildren().add(loggingTextArea);
        loggingTextArea.setEditable(false);
        getValuesMQTTAdapter().lastMessageArrivedProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Platform.runLater(() -> {
                loggingTextArea.appendText(newValue);
            });
        });
        contextToggleButton.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                Settings.getInstance().setApplicationContext(Settings.AppContext.WORLD);
                contextButtonIconView.setIcon(FontAwesomeIcon.CLOUD);
                onReConnect();
            } else {
                Settings.getInstance().setApplicationContext(Settings.AppContext.HOME);
                contextButtonIconView.setIcon(FontAwesomeIcon.HOME);
                onReConnect();
            }
        });

        Settings.getInstance().applicationContextProperty().addListener((ObservableValue<? extends Settings.AppContext> observable, Settings.AppContext oldValue, Settings.AppContext newValue) -> {
        });

        rainbowSelector.targetColorProperty().addListener(
                o -> publishRainbowColor());

        rainbowSelector.setOnButtonOffReleased((MouseEvent event) -> {
            rainbowColorOff();
        });
        rainbowSelector.setOnButtonOnReleased((MouseEvent event) -> {
            rainbowColorOn();
        });
        rainbowColorOff();
        rainbowSelector.setOn(false);
    }

    public boolean isRainbowSelectorOn() {
        return rainbowSelectorOn;
    }

    public void setRainbowSelectorOn(boolean rainbowSelectorOn) {
        this.rainbowSelectorOn = rainbowSelectorOn;
    }

    private void rainbowColorOff() {
        setRainbowSelectorOn(false);
        publishRainbowColor(Color.BLACK);
    }

    private void rainbowColorOn() {
        setRainbowSelectorOn(true);
        publishRainbowColor(rainbowSelector.getTargetColor());
    }

    private void publishRainbowColor() {
        if (isRainbowSelectorOn()) {
            publishRainbowColor(rainbowSelector.getTargetColor());
        }
    }

    private void publishRainbowColor(final Color color) {
        if (getValuesMQTTAdapter().getMqttClientController().getMqttClient() != null) {
            try {
                String colorMessage = String.format(
                        "%d,%d,%d",
                        (int) (color.getRed() * 255),
                        (int) (color.getGreen() * 255),
                        (int) (color.getBlue() * 255)
                );

                System.out.println("*** " + colorMessage);
                String topic = "sweethome/actor/indoor/rainbow";
                MqttMessage message = new MqttMessage(colorMessage.getBytes());
//            message.setRetained(true);
                getValuesMQTTAdapter().getMqttClientController().getMqttClient().publish(topic, message);
            } catch (MqttException ex) {
                Logger.getLogger(DisplayView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void checkConnectionStatus() {
        if (getValuesMQTTAdapter().isClientConnected()) {
            getValuesMQTTAdapter().setConnectionStatusProperty(SweethomeValuesMQTTAdapter.ConnectionStatus.CONNECTED);
        } else {
            getValuesMQTTAdapter().setConnectionStatusProperty(SweethomeValuesMQTTAdapter.ConnectionStatus.DISCONNECTED);
        }
    }

    private void showStatusConnected() {
        connectionStatusLabel.setText(resources.getString("connection.status.connected"));
        connectionStatusIndicator.setResult(Indicator.Result.PASS);
    }

    private void showStatusDisconnected() {
        connectionStatusLabel.setText(resources.getString("connection.status.disconnected"));
        connectionStatusIndicator.setResult(Indicator.Result.FAIL);
    }

    private void showStatusConnecting() {
        connectionStatusLabel.setText(resources.getString("connection.status.connecting"));
        connectionStatusIndicator.setResult(Indicator.Result.INDETERMINDED);
    }

    /*
     * -------------------------- ACTIONS -------------------------- 
     */
    @FXML
    public void onExit() {
        onDisconnect();
        Platform.exit();
        System.exit(0);
    }

    public SweetHomeValues getValuesDataModel() {
        if (valuesDataModel == null) {
            valuesDataModel = new SweetHomeValues();
        }
        return valuesDataModel;
    }

    public SweethomeValuesMQTTAdapter getValuesMQTTAdapter() {
        if (valuesMQTTAdapter == null) {
            MqttClientConfiguration configuration = new MqttClientConfiguration();
            configuration.setMqttClientId(UUID.randomUUID().toString());
            valuesMQTTAdapter = new SweethomeValuesMQTTAdapter(configuration, getValuesDataModel());
        }
        if (Settings.getInstance().getApplicationContext().equals(Settings.AppContext.WORLD)) {
            valuesMQTTAdapter.getMqttClientConfiguration().setMqttBrokerAddress(Settings.getInstance().getMqttBrokerAddressWorld());
            valuesMQTTAdapter.getMqttClientConfiguration().setMqttBrokerPort(Settings.getInstance().getMqttBrokerPortWorld());
        } else {
            valuesMQTTAdapter.getMqttClientConfiguration().setMqttBrokerAddress(Settings.getInstance().getMqttBrokerAddressHome());
            valuesMQTTAdapter.getMqttClientConfiguration().setMqttBrokerPort(Settings.getInstance().getMqttBrokerPortHome());
        }
        return valuesMQTTAdapter;
    }

    @FXML
    public void onReConnect() {
        try {
            getValuesMQTTAdapter().reconnect();
        } catch (MqttException ex) {
            throw new RuntimeException(ex);
        } finally {
            checkConnectionStatus();
        }
    }

    @FXML
    public void onConnect() {
        try {
            getValuesMQTTAdapter().connect();
        } catch (MqttException ex) {
            throw new RuntimeException(ex);
        } finally {
            checkConnectionStatus();
        }
    }

    @FXML
    public void onDisconnect() {
        if (getValuesMQTTAdapter() != null && getValuesMQTTAdapter().isClientConnected()) {
            try {
                getValuesMQTTAdapter().disconnect();
            } catch (MqttException ex) {
                throw new RuntimeException(ex);
            } finally {
                checkConnectionStatus();
            }
        }
    }

    @FXML
    public void onToggleDisplayMode() {
        if (displayModeToggleButton.isSelected()) {
            setOpacity(1.0);
            displayModeToggleButton.setText("Day");
        } else {
            setOpacity(0.1);
            displayModeToggleButton.setText("Night");
        }

    }

    @FXML
    public void onNextTab() {
        int index = mainTabPane.getSelectionModel().getSelectedIndex();
        if (index == mainTabPane.getTabs().size()) {
            return;
        }
        mainTabPane.getSelectionModel().select(index++);
    }

    @FXML
    public void onPreviousTab() {
        int index = mainTabPane.getSelectionModel().getSelectedIndex();
        if (index == 0) {
            return;
        }
        mainTabPane.getSelectionModel().select(index--);
    }

    @FXML
    public void onSaveSettings() {
        Settings.getInstance().saveSettings();
    }

    @FXML
    public void onRestoreSettings() {
        Settings.getInstance().restoreSettings();
    }

    private void doPublish(String topic, String payload) {
        if (!getValuesMQTTAdapter().isClientConnected()) {
            onConnect();
        } else {
            getValuesMQTTAdapter().publish(topic, payload);
        }
    }

    @FXML
    public void onSwitchOnFrontLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_FRONT_WALK_LIGHT.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffFrontLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_FRONT_WALK_LIGHT.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnFrontLightDoor() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_FRONT_MAINENTRANCE_LIGHT.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffFrontLightDoor() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_FRONT_MAINENTRANCE_LIGHT.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenLightTerrace() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_TERRACE_LIGHT.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenLightTerrace() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_TERRACE_LIGHT.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_GREENAREA_LIGHT.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_GREENAREA_LIGHT.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnLivingRoomLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_INDOOR_LIVINGROOM_WALL_LIGHT.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffLivingRoomLightAmbiente() {
        doPublish(SweethomeTopics.ACTOR_INDOOR_LIVINGROOM_WALL_LIGHT.getNameWithDefaultBaseTopic(), "OFF");
    }

    @FXML
    public void onSwitchOnGardenDeviceFountain() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_FOUNTAIN.getNameWithDefaultBaseTopic(), "ON");
    }

    @FXML
    public void onSwitchOffGardenDeviceFountain() {
        doPublish(SweethomeTopics.ACTOR_OUTDOOR_BACKYARD_FOUNTAIN.getNameWithDefaultBaseTopic(), "OFF");
    }

}
