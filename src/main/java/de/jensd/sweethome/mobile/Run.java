package de.jensd.sweethome.mobile;

import de.jensd.sweethome.view.StartDisplayView;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class Run extends Application {

    @Override
    public void init() throws Exception {
        try {
            Font.loadFont(Run.class.getResource("/de/jensd/sweethome/view/fonts/Fontfabric - Panton.otf").openStream(), 10.0);
        } catch (IOException ex) {
            Logger.getLogger(Run.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Platform.runLater(() -> {
            new StartDisplayView().start(new Stage());
        });
    }

}
